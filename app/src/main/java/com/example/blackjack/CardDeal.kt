package com.example.blackjack

import androidx.annotation.DrawableRes

//https://stackoverflow.com/questions/46600942/how-to-merge-two-sets-of-enums-in-kotlin-having-the-same-parent-interface
class CardDeal {
    /*
    * Grab all values in enum classes
    * grab two random cards from the combined enum classes
    * remove them from the associated enum
    * */

    private lateinit var cardDeck:List<Int>

    fun getCardDeck(): List<Int> {
        return this.cardDeck
    }

    fun setCardDeck(cardDeck:List<Int>) {
        this.cardDeck = cardDeck;
    }

    fun getCard() {

    }

    fun newDeal() {

    }

}