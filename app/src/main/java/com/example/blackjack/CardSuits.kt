package com.example.blackjack

enum class CardSuits {
    DIAMONDS,
    CLUBS,
    HEARTS,
    SPACES
}