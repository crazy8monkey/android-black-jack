package com.example.blackjack

import androidx.annotation.DrawableRes

enum class Spades(@DrawableRes val cardRes: Int) {
    ACE(R.drawable.ace_of_spades),
    TWO(R.drawable.two_of_spades),
    THREE(R.drawable.three_of_spades),
    FOUR(R.drawable.four_of_spades),
    FIVE(R.drawable.five_of_spades),
    SIX(R.drawable.six_of_spades),
    SEVEN(R.drawable.seven_of_spades),
    EIGHT(R.drawable.eight_of_spades),
    NINE(R.drawable.nine_of_spades),
    KING(R.drawable.king_of_spades),
    QUEEN(R.drawable.queen_of_spades),
    JACK(R.drawable.jack_of_spades)
}
