package com.example.blackjack

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import kotlin.math.log

import android.R.array
import android.widget.Button
import kotlin.random.Random
import android.widget.LinearLayout





class MainActivity : AppCompatActivity() {

    private var cardDeck = CardDeal()
    lateinit var handDeal: FrameLayout
    lateinit var dealBtn: Button
    lateinit var hitBtn: Button
    lateinit var standBtn: Button
    lateinit var dealBtnContainer: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        handDeal = findViewById(R.id.cardDealLayout)
        dealBtn = findViewById(R.id.deal)
        dealBtnContainer = findViewById(R.id.currentDealBtnContainer)
        dealBtnContainer.visibility = View.INVISIBLE
    }

    fun dealNewHand(view: View) {
        val cardOneParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(500, 500)
        cardOneParams.leftMargin = 100
        cardOneParams.topMargin = 0

        val cardTwoParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(500, 500)
        cardTwoParams.leftMargin = 200
        cardTwoParams.topMargin = 0

        val imageArray = resources.obtainTypedArray(R.array.card_deck)
        val cardOne = ImageView(this)
        val cardTwo = ImageView(this)
        val cardOneIndex = Random.nextInt(imageArray.length())
        val cardTwoIndex = Random.nextInt(imageArray.length())
        Log.d("index one", cardOneIndex.toString())
        Log.d("index two", cardTwoIndex.toString())

        cardOne.setImageResource(imageArray.getResourceId(cardOneIndex, -1))
        cardTwo.setImageResource(imageArray.getResourceId(cardTwoIndex, -1))

        handDeal.addView(cardOne, cardOneParams)
        handDeal.addView(cardTwo, cardTwoParams)

        dealBtn.visibility = View.INVISIBLE
        dealBtnContainer.visibility = View.VISIBLE
    }


}
